# AWS fan out

This project is part of the step by step guide published on: [Step by step fan-out serverless archictecture on AWS](https://medium.com/@rafaelrodriguez_68048/its-simple-with-aws-a-step-by-step-serverless-fan-out-architecture-guide-d81f5b0e0faa)

### Requirements

- AWS Account secret key and access key
- Account should have permissions to create IAM Roles, IAM Policies, API Gateway, Lambda, SQS, SNS, SNS Subscriptions
- [Terraform](https://www.terraform.io/)

### Provision

The following set of commands will create your infrastructure in eu-west-1, you can change this by adding more regions in **[variables.tf](https://gitlab.com/rodriguezrps/aws-fan-out/-/blob/master/infra/variables.tf[]([url](url)))** and creating different terraform workspaces.

```console
cd infra
terraform init
terraform workspace new test-ie
terraform apply
```

Copy the outputs of your terraform apply and execute few curl commands

```console
curl --location --request POST 'https://YOUR_API_URL/v1/market/ru/shops/123/oders' --header 'x-api-key: YOUR_API_ID'
curl --location --request POST 'https://YOUR_API_URL/v1/market/us/shops/123/oders' --header 'x-api-key: YOUR_API_ID'
```
Go to your AWS Console and check for your lambda [CloudWatch](https://eu-west-1.console.aws.amazon.com/cloudwatch/home?region=eu-west-1#logsV2:log-groups) logs.


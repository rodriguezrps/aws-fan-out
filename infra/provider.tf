provider "aws" {
    region  = var.workspace_region[terraform.workspace]
    # assume_role {
    #     role_arn = var.workspace_role[terraform.workspace]
    # }
}

output "region" {
  value = var.workspace_region[terraform.workspace]
}
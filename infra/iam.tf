##### API GATEWAY ROLES

resource "aws_iam_role" "fan_out_api_role" {
    name = "${terraform.workspace}-FanOutApiGatewayRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Effect": "Allow",
        "Principal": {
        "Service": "apigateway.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
    }
    ]
}
EOF
}

resource "aws_iam_policy" "fan_out_api_policy" {
  name        = "${terraform.workspace}-FanOutApiPolicy"
  description = "Fan Out Sample API Gateway / SNS Policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sns:Publish"
      ],
      "Effect": "Allow",
      "Resource": ["${aws_sns_topic.orders_topic.arn}"]
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "fan_out_api_role_attachment" {
  role       = aws_iam_role.fan_out_api_role.name
  policy_arn = aws_iam_policy.fan_out_api_policy.arn
}


### Lambda Role

resource "aws_iam_role" "compliance_lambda_role" {
      name = "${terraform.workspace}_compliance_lambda_role"
      assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Effect": "Allow",
        "Principal": {
        "Service": "lambda.amazonaws.com"
        },
        "Action": "sts:AssumeRole",
        "Sid": ""
    }
    ]
}
EOF
}

resource "aws_iam_role_policy" "compliance_lambda_logging" {
  name = "${terraform.workspace}_compliance_lambda_logging"
  role = aws_iam_role.compliance_lambda_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "compliance_lambda_sqs_policy" {
  name = "${terraform.workspace}_compliance_lambda_sqs_policy"
  role = aws_iam_role.compliance_lambda_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Effect": "Allow",
          "Action": [
              "sqs:*"
          ],
          "Resource": [
            "${aws_sqs_queue.compliance_queue.arn}"
          ]
      }
  ]
}
EOF
}
data "archive_file" "compliance_lambda" {
    type        = "zip"
    output_path = "../lambdas/src/dist/compliance_lambda.zip"
    source {
        content  = "${file("../lambdas/src/compliance/compliance_lambda.js")}"
        filename = "compliance_lambda.js"
    }
}

resource "aws_lambda_function" "compliance_lambda" {
    function_name = "${terraform.workspace}_compliance_lambda"
    handler = "compliance_lambda.handler"
    runtime = "nodejs12.x"
    memory_size = "128"
    timeout = "30"
    filename = data.archive_file.compliance_lambda.output_path
    source_code_hash = data.archive_file.compliance_lambda.output_base64sha256
    role = aws_iam_role.compliance_lambda_role.arn
}

resource "aws_lambda_event_source_mapping" "compliance_lambda" {
  batch_size       = 10
  event_source_arn = aws_sqs_queue.compliance_queue.arn
  function_name    = aws_lambda_function.compliance_lambda.arn
}
variable "workspace_region" {
  type    = map
  default = {
    test-ie = "eu-west-1"
    test-uk = "eu-west-2"
  }
}
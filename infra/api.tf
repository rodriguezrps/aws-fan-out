resource "aws_api_gateway_rest_api" "fan_out_api_gateway" {
    name        = "${terraform.workspace}-FAN-OUT-API"
    description = "FAN OUT SAMPLE"
    body        = data.template_file.api_swagger.rendered
}

data "template_file" "api_swagger" {
    template = "${file("api.yaml")}"
    vars = {
        fan_out_api_role  = aws_iam_role.fan_out_api_role.arn
        oders_topic_uri   = "arn:aws:apigateway:${var.workspace_region[terraform.workspace]}:sns:path//"
        orders_topic      = aws_sns_topic.orders_topic.arn  
    }
}

resource "aws_api_gateway_deployment" "fan_out_api_gateway_deployment" {
    rest_api_id = aws_api_gateway_rest_api.fan_out_api_gateway.id
    stage_name  = "v1"
    stage_description = md5(file("api.yaml"))

    variables = {
        file_hash = md5(file("api.tf"))
    }

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_api_gateway_usage_plan" "fan_out_usage_plan" {
    name = "${terraform.workspace}-FAN-OUT-USAGE-PLAN"

    api_stages {
        api_id = aws_api_gateway_rest_api.fan_out_api_gateway.id
        stage  = aws_api_gateway_deployment.fan_out_api_gateway_deployment.stage_name
    }
}

resource "aws_api_gateway_api_key" "fan_out_api_key" {
    name = "${terraform.workspace}-FAN-OUT-API-KEY"
}

resource "aws_api_gateway_usage_plan_key" "fan_out_usage_plan_key" {
    key_id        = aws_api_gateway_api_key.fan_out_api_key.id
    key_type      = "API_KEY"
    usage_plan_id = aws_api_gateway_usage_plan.fan_out_usage_plan.id
    depends_on    = [
        aws_api_gateway_deployment.fan_out_api_gateway_deployment,
    ]
}

output "fan_out_api_key" {
    value = aws_api_gateway_api_key.fan_out_api_key.value
}

output "api_url" {
    value = aws_api_gateway_deployment.fan_out_api_gateway_deployment.invoke_url
}
